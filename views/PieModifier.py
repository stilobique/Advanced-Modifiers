import bpy


class MENU_MT_PieModifiers(bpy.types.Menu):
    bl_label = "Pie Modifiers tools"

    def draw(self, context):
        layout = self.layout

        pie = layout.menu_pie()

        # Pie for Modifier Tools
        box = pie.split().box()
        box.label(text="Modifier preset")

        # ---- Subsurf setup
        row = box.column(align=True)
        data = context.scene.md_subsurf
        row.operator("modifiers.subsurf_preset", text="Subsurf",
                     icon="MOD_SUBSURF")
        split = row.split(align=True)
        split.prop(data, "subsurf_intensity", expand=True, toggle=True)

        # ---- Mirror Preset
        data = context.scene.md_vars
        row.separator()
        row.operator("modifiers.mirror_preset", text="Mirror", icon="MOD_MIRROR")
        split = row.split(align=True)
        split.prop(data, "axis_x", text="X", toggle=True)
        split.prop(data, "axis_y", text="Y", toggle=True)
        split.prop(data, "axis_z", text="Z", toggle=True)

        # ---- Edge Split Preset
        data = context.scene.md_split
        row.separator()
        row.operator("modifiers.edgesplit_preset", text="Edge Split",
                     icon="MOD_EDGESPLIT")
        split = row.split(align=True)
        split.prop(data, "angle_intensity", slider=True, toggle=True)

        # Tools modifier
        box = pie.split().box()
        box.label(text="Modifiers Tools")
        row = box.column()
        split = row.split(align=True)
        ops = "modifiers.batch_modifiers"
        split.operator(ops, text="Apply", icon='ADD').batch = "Apply"
        split.operator(ops, text="Clear", icon='PANEL_CLOSE').batch = "Remove"
        row.separator()

        data = context.scene.md_vars
        split = row.split(align=True)
        split.prop(data, "md_render", icon='RESTRICT_RENDER_OFF',
                   icon_only=True)
        split.prop(data, "md_viewport", icon='RESTRICT_VIEW_OFF',
                   icon_only=True)
        split.prop(data, "md_edit", icon='EDITMODE_HLT', icon_only=True)
        split.prop(data, "md_cage", icon='MESH_DATA', icon_only=True)

        # Info Modifier
        md_list = context.object.modifiers
        md_count = len(md_list)
        if md_count:
            box = pie.split().box()
            box.label(text="{0} modifier.s on the stack.".format(md_count))
            for md in md_list:
                box.label(text="{0}, type {1}".format(md.name,
                                                      md.type))
