import bpy


class ADVANDEDMODIFIER_PT_setting(bpy.types.AddonPreferences):
    bl_idname = "Advanced-Modifiers"

    subsurf_boolean: bpy.props.BoolProperty(
        name="Subsurf preset",
        default=True,
        )
    mirror_boolean: bpy.props.BoolProperty(
        name='Mirror',
        default=True,
        )
    noise_boolean: bpy.props.BoolProperty(
        name="Noise displace",
        default=True,
        )
    lod_boolean: bpy.props.BoolProperty(
        name="LoD generator",
        default=False,
        )
    bend_boolean: bpy.props.BoolProperty(
        name="Bend setup",
        default=True,
        )

    def draw(self, context):
        layout = self.layout
        layout.label(text="Select your presets")
        layout.prop(self, "subsurf_boolean")
        layout.prop(self, "mirror_boolean")
        layout.prop(self, "noise_boolean")
        layout.prop(self, "lod_boolean")
        layout.prop(self, "bend_boolean")
