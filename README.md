# Blender AddOn

## Description
Various script to optimized Modifier used.

## How to install
Copy files in Scripts folder to blender.
Linux :
```Linux
$HOME/.config/blender/2.76/scripts/addons/
```

```Windows
%APPDATA%\Blender Foundation\Blender\2.xx\scripts\addons\
```

### Pie menu
You can invoke the modifier Pie Menu with `Ctrl` + `SHift` + `M`.

## Resources
* [API Blender](https://www.blender.org/api/blender_python_api_2_65_5/contents.html)
* [Write AddOn](https://www.blender.org/api/blender_python_api_2_65_5/info_tutorial_addon.html)
* [WikiBooks](https://en.wikibooks.org/wiki/Blender_3D:_Noob_to_Pro/Advanced_Tutorials/Python_Scripting/Export_scripts)
* [BlenderNation](http://www.blendernation.com/2015/09/17/blender-addon-script-watcher/)
