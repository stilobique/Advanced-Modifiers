import bpy


class STILO_OT_batch_modifiers(bpy.types.Operator):
    """Apply all modifier on the selected object(s)"""
    bl_label = 'Make batch operation with modifier, apply it by default'
    bl_idname = 'modifiers.batch_modifiers'
    bl_options = {'REGISTER', 'UNDO'}

    batch: bpy.props.StringProperty(
        default='Apply'
    )

    @classmethod
    def poll(cls, context):
        return context.selected_objects is not None

    # TODO Batch has broken, work only with the first selected object.
    def execute(self, context):
        selected_obj = context.selected_objects
        state = context.scene.md_vars.expand
        for obj in selected_obj:
            if obj.type == 'MESH':
                md_list = obj.modifiers.items()
                for md in md_list:
                    try:
                        md_name = md[1].name
                        md_bpy = bpy.ops.object
                        if 'Apply' in self.batch:
                            md_bpy.modifier_apply(modifier=md_name)
                        elif 'Remove' in self.batch:
                            md_bpy.modifier_remove(modifier=md_name)
                        elif 'Expand' in self.batch:
                            md[1].show_expanded = state
                    except RuntimeError:
                        self.report({'WARNING'}, 'One (or more) modifier can be apply')
                        return {'CANCELLED'}

        if 'Expand' in self.batch:
            context.scene.md_vars.expand = not state
        return {'FINISHED'}


# TODO Refactoring and integration about this operator, a show/hide modifier.s
class STILO_OT_display_modifiers(bpy.types.Operator):
    """Control the display/Rendering with all modifiers"""
    bl_label = 'Show or Hide all modifiers in the viewport'
    bl_idname = 'modifiers.display_modifiers'
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        selected_obj = bpy.context.selected_objects
        state = context.scene.md_vars

        for obj in selected_obj:
            for md in obj.modifiers:
                md.show_render = state.md_render
                md.show_viewport = state.md_viewport
                md.show_in_editmode = state.md_edit
                md.show_on_cage = state.md_cage

        return {'FINISHED'}
